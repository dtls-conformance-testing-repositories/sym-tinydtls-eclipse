#!/bin/bash

echo -e "\e[32m[+] - Cleaning the project directory"
echo -e "\n"
make clean

echo -e "\e[32m[+] - Preparing the bitcode"
echo -e "\n"

export CPATH=/home/$(whoami)/klee/include
cd ../
make clean
sym=1 make 
cd tests/
extract-bc dtls-fuzz

echo -e "\n\e[32m[+] - Compilation has been successfuly completed!\n"



