read -p 'Experiment: ' rulevar

/home/hooman/Repos/symbolicexecution/klee/build/bin/klee --simplify-sym-indices --write-cov -disable-inlining --optimize --use-forked-solver --use-cex-cache --libc=uclibc --posix-runtime --only-output-states-covering-new --external-calls=all --max-memory-inhibit=false --search=random-path --search=nurs:covnew --max-memory=6000 --max-sym-array-size=4096 --solver-backend=z3 --max-solver-time=10s -write-sym-paths -output-dir=$rulevar ./dtls-fuzz.bc -i handshakes/nofrag -r $rulevar
