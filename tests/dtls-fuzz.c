
/* This is needed for apple */
#define __APPLE_USE_RFC_3542

#include <assert.h>
#include <dirent.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <netinet/in.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netdb.h>
#include <signal.h>
#include <stdbool.h>

#include "tinydtls.h"
#include "dtls.h"

/// Added by Hooman
#pragma clang diagnostic ignored "-Wpointer-sign"
#include "symbolic.h"
#include "records.h"
#include <getopt.h>
// Sequential Configuration
#define MAX_READ_BUFF 500
#define SYMBOLIC_BUFF_LENGTH 259
//
#define FULLHND 1

#if ISSYM
#include "klee/klee.h"
//#include "globals.h"
#endif
#include "dtls_debug.h"

////////////

#define LOG_MODULE "dtls-server"
#define LOG_LEVEL LOG_LEVEL_DTLS
//#include "dtls-log.h"

#define DEFAULT_PORT 20220

#define MAX_FILENAME_LEN 100

#define PSK2_HANDSHAKE_FOLDER "handshakes/psk2"

#define PSK_IDENTITY "Client_identity"
#define PSK_IDENTITY_LEN 15
#define PSK_KEY "\x12\x34"
#define PSK_KEY_LEN 2

char PSK_HANDSHAKE_FOLDER[] = "handshakes/nofrag";
char ECC_HANDSHAKE_FOLDER[] = "handshakes/ecc";

bool isFragmented = 0;
//////////////////////
CH0 client_hello1;
CH0 shadow_client_hello1;
CH2 client_hello2;
CH2 shadow_client_hello2;
CKE client_key_exchange;
CKE shadow_client_key_exchange;

CKEf1 client_key_exchange_frag1;
CKEf2 client_key_exchange_frag2;
CKEf1 shadow_client_key_exchange_frag1;
CKEf2 shadow_client_key_exchange_frag2;

HVR hello_verify_request;
HVR shadow_hello_verify_request;
SH server_hello;
SH shadow_server_hello;

SHf1 server_hello_frag1;
SHf1 shadow_server_hello_frag1;
SHf2 server_hello_frag2;
SHf2 shadow_server_hello_frag2;

SHD server_hello_done;
SHD shadow_server_hello_done;
CCS client_change_cipher;
CCS shadow_client_change_cipher;
FIN client_finished;
FIN shadow_client_finished;
CCS server_change_cipher;
CCS shadow_server_change_cipher;
FIN server_finished;
FIN shadow_server_finished;
/////////////////////////////

/////////////////////////////

//////////////////////
dtls_context_t *the_server_context = NULL;
dtls_context_t *the_client_context = NULL;
fd_set rfds, wfds;
struct timeval timeout;
int fd = 100, opt, result;
FILE *f = NULL;
char file_name[MAX_FILENAME_LEN], base_name[MAX_FILENAME_LEN];
int on = 1, no_of_msg = 0, len = 0;
// 0 - message for the server, 1 - message for the client
int psk_roles[] = {0 /*CH*/, 1 /*HVR*/, 0 /*CH*/, 1 /*SH*/, 1 /*SHD*/,
                   0 /*CKE*/, 0 /*CCS*/, 0 /*FIN*/, 1 /*CCS*/, 1 /*FIN*/};
int ecc_roles[] = {0 /*CH*/, 1 /*HVR*/, 0 /*CH*/,
                   1 /*SH*/, 1 /*Cert*/, 1 /*SKE*/, 1 /*CertReq*/, 1 /*SHD*/,
                   0 /*Cert*/, 0 /*CKE*/, 0 /*CertVer*/, 0 /*CCS*/, 0 /*FIN*/,
                   1 /*CCS*/, 1 /*FIN*/};
// int psk2_roles[] = {0 /*CH0 & CH2*/, 1 /*SH*/, 1 /*SHD*/,\
  //                    0 /*CKE*/, 0 /*CCS*/, 0 /*FIN*/, 1 /*CCS*/, 1 /*FIN*/};
int psk2_roles[] = {0 /*CH0 & CH2**CKE**CCS**FIN*/, 1 /*SH*/, 1 /*SHD*/, 1 /*CCS*/, 1 /*FIN*/};

int *roles; // array

OutputRequirement output_req;
char output_record[MAX_READ_BUFF];
int step = 0;
/////////////////////
static const unsigned char ecdsa_priv_key[] = {
    0xD9, 0xE2, 0x70, 0x7A, 0x72, 0xDA, 0x6A, 0x05,
    0x04, 0x99, 0x5C, 0x86, 0xED, 0xDB, 0xE3, 0xEF,
    0xC7, 0xF1, 0xCD, 0x74, 0x83, 0x8F, 0x75, 0x70,
    0xC8, 0x07, 0x2D, 0x0A, 0x76, 0x26, 0x1B, 0xD4};

static const unsigned char ecdsa_pub_key_x[] = {
    0xD0, 0x55, 0xEE, 0x14, 0x08, 0x4D, 0x6E, 0x06,
    0x15, 0x59, 0x9D, 0xB5, 0x83, 0x91, 0x3E, 0x4A,
    0x3E, 0x45, 0x26, 0xA2, 0x70, 0x4D, 0x61, 0xF2,
    0x7A, 0x4C, 0xCF, 0xBA, 0x97, 0x58, 0xEF, 0x9A};

static const unsigned char ecdsa_pub_key_y[] = {
    0xB4, 0x18, 0xB6, 0x4A, 0xFE, 0x80, 0x30, 0xDA,
    0x1D, 0xDC, 0xF4, 0xF4, 0x2E, 0x2F, 0x26, 0x31,
    0xD0, 0x43, 0xB1, 0xFB, 0x03, 0xE2, 0x2F, 0x4D,
    0x17, 0xDE, 0x43, 0xF9, 0xF9, 0xAD, 0xEE, 0x70};

/* This function is the "key store" for tinyDTLS. It is called to
 * retrieve a key for the given identity within this particular
 * session. */
static int
get_psk_info(struct dtls_context_t *ctx, const session_t *session,
             dtls_credentials_type_t type,
             const unsigned char *id, size_t id_len,
             unsigned char *result, size_t result_length)
{
  switch (type)
  {
  case DTLS_PSK_KEY:

    memcpy(result, (unsigned char *)PSK_KEY, PSK_KEY_LEN);
    return 2;

  case DTLS_PSK_IDENTITY:
    memcpy(result, (unsigned char *)PSK_IDENTITY, PSK_IDENTITY_LEN);
    return 15;
  default:
    return 0;
  }
}

static int
get_ecdsa_key(struct dtls_context_t *ctx,
              const session_t *session,
              const dtls_ecdsa_key_t **result)
{
  static const dtls_ecdsa_key_t ecdsa_key = {
      .curve = DTLS_ECDH_CURVE_SECP256R1,
      .priv_key = ecdsa_priv_key,
      .pub_key_x = ecdsa_pub_key_x,
      .pub_key_y = ecdsa_pub_key_y};

  *result = &ecdsa_key;
  return 0;
}

static int
verify_ecdsa_key(struct dtls_context_t *ctx,
                 const session_t *session,
                 const unsigned char *other_pub_x,
                 const unsigned char *other_pub_y,
                 size_t key_size)
{
  return 0;
}

#define DTLS_SERVER_CMD_CLOSE "server:close"
#define DTLS_SERVER_CMD_RENEGOTIATE "server:renegotiate"

static int
dtls_add_peer(dtls_context_t *ctx, dtls_peer_t *peer)
{
  if (peer)
  {
    // add_peer(&ctx->peers, peer);
  }
  return 0;
}

static int
read_from_peer(struct dtls_context_t *ctx,
               session_t *session, uint8_t *data, size_t len)
{
  size_t i;
  for (i = 0; i < len; i++)
    printf("%c", data[i]);
  if (len >= strlen(DTLS_SERVER_CMD_CLOSE) &&
      !memcmp(data, DTLS_SERVER_CMD_CLOSE, strlen(DTLS_SERVER_CMD_CLOSE)))
  {
    printf("server: closing connection\n");
    dtls_close(ctx, session);
    return len;
  }
  else if (len >= strlen(DTLS_SERVER_CMD_RENEGOTIATE) &&
           !memcmp(data, DTLS_SERVER_CMD_RENEGOTIATE, strlen(DTLS_SERVER_CMD_RENEGOTIATE)))
  {
    printf("server: renegotiate connection\n");
    dtls_renegotiate(ctx, session);
    return len;
  }

  return dtls_write(ctx, session, data, len);
}

// variables for handling dump output mode
static int dump_output_mode = 0;
static int dump_index = 0;

// sending is always successful
static int
send_to_peer(struct dtls_context_t *ctx,
             session_t *session, uint8_t *data, size_t len)
{
  char base_name[MAX_FILENAME_LEN], *handshake_folder;
  if (dump_output_mode)
  {
    if (ctx->h->get_psk_info != NULL)
    {
      handshake_folder = PSK_HANDSHAKE_FOLDER;
    }
    else if (ctx->h->get_ecdsa_key != NULL)
    {
      handshake_folder = ECC_HANDSHAKE_FOLDER;
    }
    else
    {
      dtls_alert("One of PSK or ECDSA must be supported");
      exit(0);
    }
    sprintf(base_name, "%s/%d", handshake_folder, dump_index++);
    FILE *f = fopen(base_name, "wb");
    fwrite(data, sizeof(uint8_t), len, f);
    fclose(f);
  }
  memset(output_record, MAX_READ_BUFF, 0);
  memcpy(output_record, data, len);
  dtls_debug_hexdump("sending", data, len);
  if (experimenttype == SERVER && ruletype == OUTPUT)
  {
    checkOutputRequirement(&client_hello1, &client_hello2, &client_key_exchange, &server_change_cipher, output_record, &output_req);
  }
  return len;
}

// we populate the address with valid data so that it passes checks
void populate_sockaddr(struct sockaddr_in *sa)
{
  sa->sin_family = AF_INET;
  sa->sin_port = htons(DEFAULT_PORT);
  sa->sin_addr.s_addr = inet_addr("127.0.0.1");
}

void make_dirs(char *path)
{
  char *sep = strchr(path, '/');
  char aux[MAX_FILENAME_LEN];
  // create subfolders
  while (sep != NULL)
  {
    strncpy(aux, path, sep - path);
    if (mkdir(aux, 0777) && errno != EEXIST)
    {
      printf("error while trying to create '%s'\n%m\n", aux);
      exit(-1);
    }
    sep = strchr(sep + 1, '/');
  }
  // then create the target folder
  if (mkdir(path, 0777) && errno != EEXIST)
  {
    printf("error while trying to create '%s'\n%m\n", path);
    exit(-1);
  }
}

// populate peer details
void populate_peer(struct dtls_context_t *ctx, session_t *session)
{
  dtls_peer_t *peer;
  dtls_handshake_parameters_t *handshake_params;
  handshake_params = dtls_handshake_new();
  handshake_params->hs_state.mseq_s = 1;
  handshake_params->hs_state.mseq_r = 0;
  handshake_params->compression = TLS_COMPRESSION_NULL;
  handshake_params->cipher = TLS_NULL_WITH_NULL_NULL;
  handshake_params->do_client_auth = 0;
  strcpy(handshake_params->keyx.psk.identity, PSK_IDENTITY);
  handshake_params->keyx.psk.id_length = PSK_IDENTITY_LEN;

  peer = dtls_new_peer(session);
  peer->role = DTLS_CLIENT;
  peer->state = DTLS_STATE_CLIENTHELLO;
  peer->handshake_params = handshake_params;
  if (dtls_add_peer(ctx, peer) < 0)
  {
    dtls_alert("cannot add peer\n");
  }
}

static int
dtls_handle_read(struct dtls_context_t *ctx, void *buf, int len, int IS_SERVER)
{

  int *fd;
  session_t session;

  fd = dtls_get_app_data(ctx);

  assert(fd);

  memset(&session, 0, sizeof(session_t));
  session.size = sizeof(session.addr);
  populate_sockaddr((struct sockaddr_in *)&session.addr.sa);

  if (len < 0)
  {
    perror("fread");
    return -1;
  }
  else
  {
    dtls_debug("got %d bytes from fake port %d\n", len,
               ntohs(session.addr.sin6.sin6_port));
  }

  return dtls_handle_message(ctx, &session, buf, len);
}

static dtls_handler_t cb = {
    .write = send_to_peer,
    .read = read_from_peer,
    .event = NULL,
    .get_psk_info = get_psk_info,
    .get_ecdsa_key = get_ecdsa_key,
    .verify_ecdsa_key = verify_ecdsa_key};

static dtls_handler_t sb = {
    .write = send_to_peer,
    .read = read_from_peer,
    .event = NULL,
    .get_psk_info = get_psk_info,
    .get_ecdsa_key = get_ecdsa_key,
    .verify_ecdsa_key = verify_ecdsa_key};

void printusage(char *argv)
{
  puts("\nFor help:");
  printf("Usage: %s [-h]\n\n", argv);
  puts("\nFor Symbolic Constraint Testing:");
  printf("Usage: %s [-i DIR] [-r constraint] [-f] \n\n", argv);
  puts("For Handshake Packet Generation:");
  printf("Usage: %s [-n] \n\n", argv);

  puts("    Constraint:\t\t\t\tDescription:");
  puts("----------------------------------------------------------------------\n");
  puts("[+] The List of Record-level Constraints:\n");

  puts("[*] ctypevalidity-server\t\tContent Type Validity for the server side");
  puts("[*] ctypevalidity-client\t\tContent Type Validity for the client side");
  puts("[*] recversion-server\t\t\tRecord Version Validity for the server side");
  puts("[*] recversion-client\t\t\tRecord Version Validity for the client side");
  puts("[*] epoch-server\t\t\tEpoch Validity for the server side");
  puts("[*] epoch-client\t\t\tEpoch Validity for the client side");
  puts("[*] rsequniqueness-server\t\tRecord Sequence Uniqueness for the server side");
  puts("[*] rsequniqueness-client\t\tRecord Sequence Uniqueness for the client side");
  puts("[*] rseqwindowing-server\t\tRecord Sequence Windowing for the server side");
  puts("[*] rseqwindowing-client\t\tRecord Sequence Windowing for the client side");
  puts("[*] rlenvalidity-server\t\t\tRecord Length Validity for the server side");
  puts("[*] rlenvalidity-client\t\t\tRecord Length Validity for the client side");

  puts("\n[+] The List of Fragment-level Constraints:\n");

  puts("[*] htypevalidity-server\t\tHandshake Type Validity for the server side");
  puts("[*] htypevalidity-client\t\tHandshake Type Validity for the client side");
  puts("[*] mlenvalidity-server\t\t\tMessage Length Validity for the server side");
  puts("[*] mlenvalidity-client\t\t\tMessage Length Validity for the client side");
  puts("[*] mseqvalidity-server\t\t\tMessage Sequence Validity for the server side");
  puts("[*] mseqvalidity-client\t\t\tMessage Sequence Validity for the client side");
  puts("[*] flenvalidity-server\t\t\tFragment Length Validity for the server side");
  puts("[*] flenvalidity-client\t\t\tFragment Length Validity for the client side");
  puts("[*] fragoffset-server\t\t\tFragment Offset Validity for the server side");
  puts("[*] fragoffset-client\t\t\tFragment Offset Validity for the client side");
  puts("[*] flenmleneq-server\t\t\tEquality of Fragment Length and Message Length for the server side");
  puts("[*] flenmleneq-client\t\t\tEquality of Fragment Length and Message Length for the client side");
  puts("[*] bytecontain-server\t\t\tIs Fragmentation done Correctly for the server side");
  puts("[*] bytecontain-client\t\t\tIs Fragmentation done Correctly for the client side");
  puts("[*] mseqeq-server\t\t\tMessage Sequence Equality in all Fragments of the same message for the server side");
  puts("[*] mseqeq-client\t\t\tMessage Sequence Equality in all Fragments of the same message for the client side");
  puts("[*] mleneq-server\t\t\tMessage Length Equality in all Fragments of the same message for the server side");
  puts("[*] mleneq-client\t\t\tMessage Length Equality in all Fragments of the same message for the client side");

  puts("\n[+] The List of Message-level Constraints:\n");

  puts("[*] handversion-server\t\t\tHandshake Version Validity for the server side");
  puts("[*] handversion-client\t\t\tHandshake Version Validity for the client side");
  puts("[*] cookielenvalidity\t\t\tCookie Length Validity for the server side");
  puts("[*] cookielenvalidity\t\t\tCookie Length Validity for the client side");
  puts("[*] sessionidlenvalidity-server\t\tSession ID Length Validity for the server side");
  puts("[*] sessionidlenvalidity-client\t\tSession ID Length Validity for the client side");
  puts("[*] extlenvalidity-server\t\tExtension Length Validity for the server side");
  puts("[*] extlenvalidity-client\t\tExtension Length Validity for the client side");

  puts("\n[+] The List of Output Constraints:\n");

  puts("[*] hvrmseqoutput-server\t\t\tHeloVerifyRequest Message Sequence Output Validity for the server side");
  puts("[*] shmseqoutput-server\t\t\tHeloVerifyRequest Message Sequence Output Validity for the server side");

  exit(-1);
}

int perform_sequential_handshake(const uint8_t *record, size_t size, char *crypt, int packet_order)
{

  dtls_context_t *the_server_context = NULL;
  dtls_context_t *the_client_context = NULL;
  dtls_context_t *the_client2_context = NULL;
  dtls_context_t *the_server2_context = NULL;
  bool gate = 0;

  int fd = 100, result;
  FILE *f = NULL;
  static uint8_t buf[MAX_READ_BUFF];
  char file_name[MAX_FILENAME_LEN], base_name[MAX_FILENAME_LEN];
  int no_of_msg = 0, len = 0;
  // 0 - message for the server, 1 - message for the client
  int psk_roles[] = {0 /*CH*/, 1 /*HVR*/, 0 /*CH*/, 1 /*SH*/, 1 /*SHD*/,
                     0 /*CKE*/, 0 /*CCS*/, 0 /*FIN*/, 1 /*CCS*/, 1 /*FIN*/};
  int ecc_roles[] = {0 /*CH*/, 1 /*HVR*/, 0 /*CH*/,
                     1 /*SH*/, 1 /*Cert*/, 1 /*SKE*/, 1 /*CertReq*/, 1 /*SHD*/,
                     0 /*Cert*/, 0 /*CKE*/, 0 /*CertVer*/, 0 /*CCS*/, 0 /*FIN*/,
                     1 /*CCS*/, 1 /*FIN*/};
  int psk2_roles[] = {0 /*CH0 & CH2*/, 1 /*SH*/, 1 /*SHD*/,
                      0 /*CKE*/, 0 /*CCS*/, 0 /*FIN*/, 1 /*CCS*/, 1 /*FIN*/};

  int *roles; // array

  //////////////////
  static uint8_t sym_buf[SYMBOLIC_BUFF_LENGTH]; // buffer for storing records which is going to be executed symbolically

  if (strcmp(crypt, "psk") == 0)
  {
    sprintf(base_name, "%s/", PSK_HANDSHAKE_FOLDER);
    if (dump_output_mode)
    {
      make_dirs(PSK_HANDSHAKE_FOLDER);
    }
    roles = psk_roles;
    no_of_msg = sizeof(psk_roles) / sizeof(int);
    // disable ECDH
    cb.get_ecdsa_key = NULL;
    cb.verify_ecdsa_key = NULL;
    sb.get_ecdsa_key = NULL;
    sb.verify_ecdsa_key = NULL;
  }
  else if (strcmp(crypt, "ecc") == 0)
  {
    sprintf(base_name, "%s/", ECC_HANDSHAKE_FOLDER);
    if (dump_output_mode)
    {
      make_dirs(ECC_HANDSHAKE_FOLDER);
    }
    roles = ecc_roles;
    no_of_msg = sizeof(ecc_roles) / sizeof(int);
    // disable PSK
    cb.get_psk_info = NULL;
    sb.get_psk_info = NULL;
  }
  else if (strcmp(crypt, "psk2") == 0)
  {
    sprintf(base_name, "%s/", PSK2_HANDSHAKE_FOLDER);
    if (dump_output_mode)
    {
      printf("Dump Mode is not supported\n");
      exit(0);
    }
    else
    {
      roles = psk2_roles;
      no_of_msg = sizeof(psk2_roles) / sizeof(int);
      cb.get_ecdsa_key = NULL;
      cb.verify_ecdsa_key = NULL;
      sb.get_ecdsa_key = NULL;
      sb.verify_ecdsa_key = NULL;
    }
  }
  else
  {
    dtls_alert("Invalid crypt value: Use \"psk\" or \"ecc\"\n");
    return 0;
  }

  dtls_init();
  the_server_context = dtls_new_context(&fd);
  dtls_set_handler(the_server_context, &sb);

  the_client_context = dtls_new_context(&fd);
  dtls_set_handler(the_client_context, &cb);

  the_server2_context = dtls_new_context(&fd);
  dtls_set_handler(the_server2_context, &sb);

  the_client2_context = dtls_new_context(&fd);
  dtls_set_handler(the_client2_context, &cb);

  session_t session;
  memset(&session, 0, sizeof(session_t));
  session.size = sizeof(session.addr);
  populate_sockaddr((struct sockaddr_in *)&session.addr.sa);

  // this should kick-start the client
  dtls_connect(the_client_context, &session);

#if FULLHND
  for (int i = 0; i < no_of_msg; i++)
  {
#else
  for (int i = 0; i < packet_order + 1; i++)
  {
#endif
    printf("Entering for loop: %d\n", i);
    sprintf(file_name, "%s%d", base_name, i);

    if (i != packet_order)
    {
      f = fopen(file_name, "rb");
      len = fread(buf, sizeof *buf, MAX_READ_BUFF, f);
      printf("\n\nLen: %d: \n\n", len);
      fclose(f);

      if (f == NULL)
      {
        dtls_alert("Couldn't open %s\n", file_name);
        break;
      }
      f = NULL;
      if (gate == 1)
      {
        if (!roles[i])
        {
          // server role (server should process it)
          result = dtls_handle_read(the_server2_context, buf, len, 1);
        }
        else
        {
          // client role (client should process it)
          result = dtls_handle_read(the_client2_context, buf, len, 0);
        }
      }
      else
      {
        if (!roles[i])
        {
          // server role (server should process it)
          result = dtls_handle_read(the_server_context, buf, len, 1);
        }
        else
        {
          // client role (client should process it)
          result = dtls_handle_read(the_client_context, buf, len, 0);
        }
      }
    }
    else
    {
      memcpy(sym_buf, record, size);
      len = size;
      printf("\n\nelse Len: %d: \n\n", len);

      ////////////////////////////// Here we start to make just some bytes symbolic

#if ISSYM

      printf("Time to make it Symbolic: \n");
      printf("\n\n\n\n\n\n\n\n\n\n\n\n\n");

      uint8_t shadowbuf[MAX_READ_BUFF];
      memset(shadowbuf, 0, MAX_READ_BUFF);

      for (int i = 0; i < len; i++) // Copy the contents of buffer into a shadow buffer
      {
        shadowbuf[i] = sym_buf[i];
      }

      if (strcmp(crypt, "psk") == 0) //Manual intervention for PSK Cipher Suites
      {
        if (i == 0)
        {
          //IntervenePSKCH0(sym_buf, shadowbuf, len);
        }
        else if (i == 2)
        {
          //IntervenePSKCH2(sym_buf, shadowbuf, len);
        }
      }

      else if (strcmp(crypt, "psk2") == 0) //Manual intervention for CH0CH2 Concatanation PSK
      {
        if (i == 0)
        {
          printf("Calling PSK2CH\n");
          //IntervenePSK2CH(sym_buf, shadowbuf, len);
        }
      }

#endif
      //////////////////////////////////////////////////
      if (!roles[i])
      {
        // server role (server should process it)
        result = dtls_handle_read(the_server_context, sym_buf, len, 1);
        *the_server2_context = *the_server_context;
        *the_client2_context = *the_client_context;
        gate = 1;
      }
      else
      {
        // client role (client should process it)
        result = dtls_handle_read(the_client_context, sym_buf, len, 0);
      }
    }

    if (result)
    {
      char response[] = "Couldn't handle message\n";
      // strcat( response, argv[i+1]);
      // strcat( response, "\n");

      dtls_alert("%s", response);
      //break;
    }
  }

  ////////////////////////////////////////////////////////////////////////////////////////////////

  for (int i = 6; i < no_of_msg; i++)
  {
    printf("\n\nRehandshake with Saved Context: %d \n", i);
    sprintf(file_name, "%s%d", base_name, i);

    f = fopen(file_name, "rb");
    len = fread(buf, sizeof *buf, MAX_READ_BUFF, f);
    printf("\n\nLen: %d: \n\n", len);
    fclose(f);

    if (!roles[i])
    {
      // server role (server should process it)
      result = dtls_handle_read(the_server_context, buf, len, 1);
    }
    else
    {
      // client role (client should process it)
      result = dtls_handle_read(the_client_context, buf, len, 0);
    }
  }

  //////////////////////////////////////////////////////////////////////////////////////////////////

  //dtls_free_context(the_server_context);
  //dtls_free_context(the_client_context);
  //dtls_free_context(the_server2_context);
  //dtls_free_context(the_client2_context);
  return 0;
}

int perform_unified_handshake_frag()
{

  dtls_init();
  the_server_context = dtls_new_context(&fd);
  dtls_set_handler(the_server_context, &sb);

  the_client_context = dtls_new_context(&fd);
  dtls_set_handler(the_client_context, &cb);

  session_t session;
  memset(&session, 0, sizeof(session_t));
  session.size = sizeof(session.addr);
  populate_sockaddr((struct sockaddr_in *)&session.addr.sa);
  // this should kick-start the client
  dtls_connect(the_client_context, &session);

  for (int i = 0; i < 12; i++)
  {
    printf("\n\ni : %d \n\n", i);
    switch (i)
    {
    case 0:
      if (experimenttype == SERVER || approach == None)
      {
        result = dtls_handle_read(the_server_context, &client_hello1, CH0_size, 1);
        if (result != 0)
        {
          printf("CH0 Process Failure!: return: %d\n", result);
          goto error;
        }
        printf("Processing CH0 was successful!!\n\n");
      }
      else
      {
        puts("Processing CH0 was Omitted!\n");
      }
      break;
    case 2:
      if (experimenttype == SERVER || approach == None)
      {
        result = dtls_handle_read(the_server_context, &client_hello2, CH2_size, 1);
        if (result != 0)
        {
          printf("CH2 Process Failure!: return: %d\n", result);
          goto error;
        }
        printf("Processing CH2 was successful!!\n\n");
      }
      else
      {
        puts("Processing CH2 was Omitted!\n");
      }
      break;
    case 6:
      if (experimenttype == SERVER || approach == None)
      {

        result = dtls_handle_read(the_server_context, &client_key_exchange_frag1, CKE_frag1_size, 1);
        if (result != 0)
        {
          printf("Fragmented CKE f1 Process Failure!: return: %d\n", result);
          goto error;
        }
        printf("Processing Fragmented CKE f1 was successful!!\n\n");
      }
      else
      {
        puts("Processing CKE f1 was Omitted!\n");
      }
      break;
    case 7:
      if (experimenttype == SERVER || approach == None)
      {

        result = dtls_handle_read(the_server_context, &client_key_exchange_frag2, CKE_frag2_size, 1);
        if (result != 0)
        {
          printf("Fragmented CKE f2 Process Failure!: return: %d\n", result);
          goto error;
        }
        printf("Processing Fragmented CKE f2 was successful!!\n\n");
      }
      else
      {
        puts("Processing CKE f2 was Omitted!\n");
      }
      break;
    case 8:
      if (experimenttype == SERVER || approach == None)
      {
        result = dtls_handle_read(the_server_context, &client_change_cipher, CCS_size, 1);
        if (result != 0)
        {
          printf("CCC Process Failure!: return: %d\n", result);
          goto error;
        }
        printf("Processing CCC was successful!!\n\n");
      }
      else
      {
        puts("Processing CCC was Omitted!\n");
      }
      break;
    case 9:
      if (experimenttype == SERVER || approach == None)
      {
        result = dtls_handle_read(the_server_context, &client_finished, FIN_size, 1);
        if (result != 0)
        {
          printf("CFI Process Failure!: return: %d\n", result);
          goto error;
        }
        printf("Processing CFI was successful!!\n\n");
      }
      else
      {
        puts("Processing CFI was Omitted!\n");
      }
      break;
    case 1:
      if (experimenttype == CLIENT || approach == None)
      {
        result = dtls_handle_read(the_client_context, &hello_verify_request, HVR_size, 1);
        if (result != 0)
        {
          printf("HVR Process Failure!: return: %d\n", result);
          goto error;
        }
        printf("Processing HVR was successful!!\n\n");
      }
      else
      {
        puts("Processing HVR was Omitted!\n");
      }
      break;
    case 3:
      if (experimenttype == CLIENT || approach == None)
      {
        result = dtls_handle_read(the_client_context, &server_hello_frag1, SH_frag1_size, 1);
        if (result != 0)
        {
          printf("Fragmented SH f1 Process Failure!: return: %d\n", result);
          goto error;
        }
        printf("Processing Fragmented SH f1 was successful!!\n\n");
      }
      else
      {
        puts("Processing SH f1 was Omitted!\n");
      }
      break;
    case 4:
      if (experimenttype == CLIENT || approach == None)
      {
        result = dtls_handle_read(the_client_context, &server_hello_frag2, SH_frag2_size, 1);
        if (result != 0)
        {
          printf("Fragmented SH f2 Process Failure!: return: %d\n", result);
          goto error;
        }
        printf("Processing Fragmented SH f2 was successful!!\n\n");
      }
      else
      {
        puts("Processing SH f2 was Omitted!\n");
      }
      break;
    case 5:
      if (experimenttype == CLIENT || approach == None)
      {
        result = dtls_handle_read(the_client_context, &server_hello_done, SHD_size, 1);
        if (result != 0)
        {
          printf("SHD Process Failure!: return: %d\n", result);
          goto error;
        }
        printf("Processing SHD was successful!!\n\n");
      }
      else
      {
        puts("Processing SHD was Omitted!\n");
      }
      break;
    case 10:
      if (experimenttype == CLIENT || approach == None)
      {
        result = dtls_handle_read(the_client_context, &server_change_cipher, CCS_size, 1);
        if (result != 0)
        {
          printf("SCC Process Failure!: return: %d\n", result);
          goto error;
        }
        printf("Processing SCC was successful!!\n\n");
      }
      else
      {
        puts("Processing SCC was Omitted!\n");
      }
      break;
    case 11:
      if (experimenttype == CLIENT || approach == None)
      {
        result = dtls_handle_read(the_client_context, &server_finished, FIN_size, 1);
        if (result != 0)
        {
          printf("SFI Process Failure!: return: %d\n", result);
          goto error;
        }
        printf("Processing SFI was successful!!\n\n");
      }
      else
      {
        puts("Processing SFI was Omitted!\n");
      }
      break;
    default:
      break;
    }
  }
error:
  if (f != NULL)
  {
    fclose(f);
  }
  return result;
}

int perform_unified_handshake_nofrag()
{
  // experimenttype = SERVER;
  // approach = CONFORMANCE;
  dtls_init();
  the_server_context = dtls_new_context(&fd);
  dtls_set_handler(the_server_context, &sb);

  the_client_context = dtls_new_context(&fd);
  dtls_set_handler(the_client_context, &cb);

  session_t session;
  memset(&session, 0, sizeof(session_t));
  session.size = sizeof(session.addr);
  populate_sockaddr((struct sockaddr_in *)&session.addr.sa);
  // this should kick-start the client
  dtls_connect(the_client_context, &session);
  step++;
  for (int i = 0; i < no_of_msg; i++)
  {
    step = i + 1;
    printf("\n\ni : %d \n\n", i);
    switch (i)
    {
    case 0:
      if (experimenttype == SERVER || approach == None)
      {
        result = dtls_handle_read(the_server_context, &client_hello1, CH0_size, 1);
        if (result != 0)
        {
          printf("CH0 Process Failure!: return: %d\n", result);
          goto error;
        }
        printf("Processing CH0 was successful!!\n\n");
      }
      else
      {
        puts("Processing CH0 was Omitted!\n");
      }
      break;
    case 2:
      if (experimenttype == SERVER || approach == None)
      {
        result = dtls_handle_read(the_server_context, &client_hello2, CH2_size, 1);
        if (result != 0)
        {
          printf("CH2 Process Failure!: return: %d\n", result);
          goto error;
        }
        printf("Processing CH2 was successful!!\n\n");
      }
      else
      {
        puts("Processing CH2 was Omitted!\n");
      }
      break;
    case 5:
      if (experimenttype == SERVER || approach == None)
      {

        result = dtls_handle_read(the_server_context, &client_key_exchange, CKE_unfrag_size, 1);
        if (result != 0)
        {
          printf("CKE Process Failure!: return: %d\n", result);
          goto error;
        }
        printf("Processing CKE was successful!!\n\n");
      }
      else
      {
        puts("Processing CKE was Omitted!\n");
      }
      break;
    case 6:
      if (experimenttype == SERVER || approach == None)
      {
        result = dtls_handle_read(the_server_context, &client_change_cipher, CCS_size, 1);
        if (result != 0)
        {
          printf("CCC Process Failure!: return: %d\n", result);
          goto error;
        }
        printf("Processing CCC was successful!!\n\n");
      }
      else
      {
        puts("Processing CCC was Omitted!\n");
      }
      break;
    case 7:
      if (experimenttype == SERVER || approach == None)
      {
        result = dtls_handle_read(the_server_context, &client_finished, FIN_size, 1);
        if (result != 0)
        {
          printf("CFI Process Failure!: return: %d\n", result);
          goto error;
        }
        printf("Processing CFI was successful!!\n\n");
      }
      else
      {
        puts("Processing CFI was Omitted!\n");
      }
      break;
    case 1:
      if (experimenttype == CLIENT || approach == None)
      {
        result = dtls_handle_read(the_client_context, &hello_verify_request, HVR_size, 1);
        if (result != 0)
        {
          printf("HVR Process Failure!: return: %d\n", result);
          goto error;
        }
        printf("Processing HVR was successful!!\n\n");
      }
      else
      {
        puts("Processing HVR was Omitted!\n");
      }
      break;
    case 3:
      if (experimenttype == CLIENT || approach == None)
      {

        result = dtls_handle_read(the_client_context, &server_hello, SH_unfrag_size, 1);
        if (result != 0)
        {
          printf("SH Process Failure!: return: %d\n", result);
          goto error;
        }
        printf("Processing SH was successful!!\n\n");
      }
      else
      {
        puts("Processing SH was Omitted!\n");
      }
      break;
    case 4:
      if (experimenttype == CLIENT || approach == None)
      {
        result = dtls_handle_read(the_client_context, &server_hello_done, SHD_size, 1);
        if (result != 0)
        {
          printf("SHD Process Failure!: return: %d\n", result);
          goto error;
        }
        printf("Processing SHD was successful!!\n\n");
      }
      else
      {
        puts("Processing SHD was Omitted!\n");
      }
      break;
    case 8:
      if (experimenttype == CLIENT || approach == None)
      {
        result = dtls_handle_read(the_client_context, &server_change_cipher, CCS_size, 1);
        if (result != 0)
        {
          printf("SCC Process Failure!: return: %d\n", result);
          goto error;
        }
        printf("Processing SCC was successful!!\n\n");
      }
      else
      {
        puts("Processing SCC was Omitted!\n");
      }
      break;
    case 9:
      if (experimenttype == CLIENT || approach == None)
      {
        result = dtls_handle_read(the_client_context, &server_finished, FIN_size, 1);
        if (result != 0)
        {
          printf("SFI Process Failure!: return: %d\n", result);
          goto error;
        }
        printf("Processing SFI was successful!!\n\n");
      }
      else
      {
        puts("Processing SFI was Omitted!\n");
      }
      break;
    default:
      break;
    }
  }

error:
  if (f != NULL)
  {
    fclose(f);
  }
  return result;
}

int load_psk_packets(char *crypt, char *rule)
{
  static uint8_t CH0_buf[CH0_size];
  uint8_t shadow_CH0[CH0_size];
  static uint8_t CH2_buf[CH2_size];
  uint8_t shadow_CH2[CH2_size];
  static uint8_t CKE_buf[CKE_unfrag_size];
  uint8_t shadow_CKE[CKE_unfrag_size];
  static uint8_t CKE_buf_frag1[CKE_frag1_size];
  static uint8_t CKE_buf_frag2[CKE_frag2_size];
  static uint8_t CCC_buf[CCS_size];
  uint8_t shadow_CCC[CCS_size];
  static uint8_t CFI_buf[FIN_size];
  uint8_t shadow_CFI[FIN_size];
  static uint8_t HVR_buf[HVR_size];
  uint8_t shadow_HVR[HVR_size];
  static uint8_t SH_buf[SH_unfrag_size];
  uint8_t shadow_SH[SH_unfrag_size];
  static uint8_t SH_buf_frag1[SH_frag1_size];
  static uint8_t SH_buf_frag2[SH_frag2_size];
  static uint8_t SHD_buf[SHD_size];
  uint8_t shadow_SHD[SHD_size];
  static uint8_t SCC_buf[CCS_size];
  uint8_t shadow_SCC[CCS_size];
  static uint8_t SFI_buf[FIN_size];
  uint8_t shadow_SFI[FIN_size];

  memset(shadow_CH0, 0, CH0_size);
  memset(shadow_HVR, 0, HVR_size);
  memset(shadow_CH2, 0, CH2_size);
  memset(shadow_SH, 0, SH_unfrag_size);
  //memset(shadow_SH_frag, 0, SH_frag_size);
  memset(shadow_SHD, 0, SHD_size);
  memset(shadow_CKE, 0, CKE_unfrag_size);
  //memset(shadow_CKE_frag, 0, CKE_frag_size);
  memset(shadow_CCC, 0, CCS_size);
  memset(shadow_CFI, 0, FIN_size);
  memset(shadow_SCC, 0, CCS_size);
  memset(shadow_SFI, 0, FIN_size);

  if (strcmp(crypt, "psk") != 0)
  {
    dtls_alert("Only PSK is supported already");
    return 0;
  }

  if (dump_output_mode)
  {
    printf("Dump Mode is not supported for unified handshake\n");
    return 0;
  }

  sprintf(base_name, "%s/", PSK_HANDSHAKE_FOLDER);

  roles = psk_roles;
  no_of_msg = sizeof(psk_roles) / sizeof(int);
  cb.get_ecdsa_key = NULL;
  cb.verify_ecdsa_key = NULL;
  sb.get_ecdsa_key = NULL;
  sb.verify_ecdsa_key = NULL;

  if (!isFragmented)
  {
    sprintf(file_name, "%s%d", base_name, 0);
    load_record(file_name, CH0_buf, CH0_size);
    memcpy(shadow_CH0, CH0_buf, CH0_size);
    ///
    sprintf(file_name, "%s%d", base_name, 2);
    load_record(file_name, CH2_buf, CH2_size);
    memcpy(shadow_CH2, CH2_buf, CH2_size);
    ///

    sprintf(file_name, "%s%d", base_name, 5);
    load_record(file_name, CKE_buf, CKE_unfrag_size);
    memcpy(shadow_CKE, CKE_buf, CKE_unfrag_size);

    ///
    sprintf(file_name, "%s%d", base_name, 6);
    load_record(file_name, CCC_buf, CCS_size);
    memcpy(shadow_CCC, CCC_buf, CCS_size);
    ///
    sprintf(file_name, "%s%d", base_name, 7);
    load_record(file_name, CFI_buf, FIN_size);
    memcpy(shadow_CFI, CFI_buf, FIN_size);
    //////
    sprintf(file_name, "%s%d", base_name, 1);
    load_record(file_name, HVR_buf, HVR_size);
    memcpy(shadow_HVR, HVR_buf, HVR_size);
    ///

    sprintf(file_name, "%s%d", base_name, 3);
    load_record(file_name, SH_buf, SH_unfrag_size);
    memcpy(shadow_SH, SH_buf, SH_unfrag_size);

    ///
    sprintf(file_name, "%s%d", base_name, 4);
    load_record(file_name, SHD_buf, SHD_size);
    memcpy(shadow_SHD, SHD_buf, SHD_size);
    ///
    sprintf(file_name, "%s%d", base_name, 8);
    load_record(file_name, SCC_buf, CCS_size);
    memcpy(shadow_SCC, SCC_buf, CCS_size);
    ///
    sprintf(file_name, "%s%d", base_name, 9);
    load_record(file_name, SFI_buf, FIN_size);
    memcpy(shadow_SFI, SFI_buf, FIN_size);
  }
  else
  {
    sprintf(file_name, "%s%d", base_name, 0);
    load_record(file_name, CH0_buf, CH0_size);
    memcpy(shadow_CH0, CH0_buf, CH0_size);
    ///
    sprintf(file_name, "%s%d", base_name, 2);
    load_record(file_name, CH2_buf, CH2_size);
    memcpy(shadow_CH2, CH2_buf, CH2_size);
    ///

    sprintf(file_name, "%s%d", base_name, 6);
    load_record(file_name, CKE_buf_frag1, CKE_frag1_size);
    // memcpy(shadow_CKE_frag, CKE_buf_frag, CKE_frag_size);
    sprintf(file_name, "%s%d", base_name, 7);
    load_record(file_name, CKE_buf_frag2, CKE_frag2_size);

    ///
    sprintf(file_name, "%s%d", base_name, 8);
    load_record(file_name, CCC_buf, CCS_size);
    memcpy(shadow_CCC, CCC_buf, CCS_size);
    ///
    sprintf(file_name, "%s%d", base_name, 9);
    load_record(file_name, CFI_buf, FIN_size);
    memcpy(shadow_CFI, CFI_buf, FIN_size);
    //////
    sprintf(file_name, "%s%d", base_name, 1);
    load_record(file_name, HVR_buf, HVR_size);
    memcpy(shadow_HVR, HVR_buf, HVR_size);
    ///

    sprintf(file_name, "%s%d", base_name, 3);
    load_record(file_name, SH_buf_frag1, SH_frag1_size);
    // memcpy(shadow_SH_frag, SH_buf_frag, SH_frag_size);
    sprintf(file_name, "%s%d", base_name, 4);
    load_record(file_name, SH_buf_frag2, SH_frag2_size);

    ///
    sprintf(file_name, "%s%d", base_name, 5);
    load_record(file_name, SHD_buf, SHD_size);
    memcpy(shadow_SHD, SHD_buf, SHD_size);
    ///
    sprintf(file_name, "%s%d", base_name, 10);
    load_record(file_name, SCC_buf, CCS_size);
    memcpy(shadow_SCC, SCC_buf, CCS_size);
    ///
    sprintf(file_name, "%s%d", base_name, 11);
    load_record(file_name, SFI_buf, FIN_size);
    memcpy(shadow_SFI, SFI_buf, FIN_size);
  }

  parse_record(CH0_buf, &client_hello1);
  shadow_client_hello1 = client_hello1;
  parse_record(CH2_buf, &client_hello2);
  shadow_client_hello2 = client_hello2;
  if (isFragmented)
  {
    parse_record(CKE_buf_frag1, &client_key_exchange_frag1);
    shadow_client_key_exchange_frag1 = client_key_exchange_frag1;
    parse_record(CKE_buf_frag2, &client_key_exchange_frag2);
    shadow_client_key_exchange_frag2 = client_key_exchange_frag2;

    parse_record(SH_buf_frag1, &server_hello_frag1);
    shadow_server_hello_frag1 = server_hello_frag1;
    parse_record(SH_buf_frag2, &server_hello_frag2);
    shadow_server_hello_frag2 = server_hello_frag2;
  }
  else
  {
    parse_record(CKE_buf, &client_key_exchange);
    shadow_client_key_exchange = client_key_exchange;

    parse_record(SH_buf, &server_hello);
    shadow_server_hello = server_hello;
  }

  parse_record(CCC_buf, &client_change_cipher);
  shadow_client_change_cipher = client_change_cipher;
  parse_record(CFI_buf, &client_finished);
  shadow_client_finished = client_finished;
  //
  parse_record(HVR_buf, &hello_verify_request);
  shadow_hello_verify_request = hello_verify_request;

  parse_record(SHD_buf, &server_hello_done);
  shadow_server_hello_done = server_hello_done;
  parse_record(SCC_buf, &server_change_cipher);
  shadow_server_change_cipher = server_change_cipher;
  parse_record(SFI_buf, &server_finished);
  shadow_server_finished = server_finished;

#if ISSYM
  ruletype = INPUT_VALIDITY;
  if (strcmp(rule, "ctypevalidity-server") == 0)
  {
    experimenttype = SERVER;
    approach = CONFORMANCE;
    printf("ContentType Server Rule:\n\n");
    contentTypeServer(&client_hello1, &client_hello2, &client_key_exchange, &client_change_cipher);
  }
  else if (strcmp(rule, "ctypevalidity-client") == 0)
  {
    experimenttype = CLIENT;
    approach = CONFORMANCE;
    printf("ContentType Client Rule:\n\n");
    contentTypeClient(&hello_verify_request, &server_hello, &server_hello_done, &server_change_cipher);
  }
  else if (strcmp(rule, "recversion-server") == 0)
  {
    experimenttype = SERVER;
    approach = CONFORMANCE;
    printf("Record Version Server Rule:\n\n");
    recordVersionServer(&client_hello2, &client_key_exchange, &client_change_cipher);
  }
  else if (strcmp(rule, "recversion-client") == 0)
  {
    experimenttype = CLIENT;
    approach = CONFORMANCE;
    printf("Record Version Client Rule:\n\n");
    recordVersionClient(&hello_verify_request, &server_hello, &server_hello_done, &server_change_cipher);
  }
  else if (strcmp(rule, "epoch-server") == 0)
  {
    experimenttype = SERVER;
    approach = CONFORMANCE;
    printf("Epoch Rule for the Server Side:\n\n");
    epochServer(&client_hello1, &client_hello2, &client_key_exchange, &client_change_cipher);
  }
  else if (strcmp(rule, "epoch-client") == 0)
  {
    experimenttype = CLIENT;
    approach = CONFORMANCE;
    printf("Epoch Rule for the Client Side:\n\n");
    epochClient(&hello_verify_request, &server_hello, &server_hello_done, &server_change_cipher);
  }
  else if (strcmp(rule, "rsequniqueness-server") == 0)
  {
    experimenttype = SERVER;
    approach = CONFORMANCE;
    printf("Record Sequence Number Rule 2 for the Server Side:\n\n");
    sequenceUniquenessServer(&client_hello2, &client_key_exchange, &client_change_cipher);
  }
  else if (strcmp(rule, "rsequniqueness-client") == 0)
  {
    experimenttype = CLIENT;
    approach = CONFORMANCE;
    printf("Record Sequence Number Rule 2 for the Client Side:\n\n");
    sequenceUniquenessClient(&hello_verify_request, &server_hello, &server_hello_done, &server_change_cipher);
  }
  else if (strcmp(rule, "rseqwindowing-server") == 0)
  {
    experimenttype = SERVER;
    approach = CONFORMANCE;
    printf("Record Sequence Number Rule 3 for the Server Side:\n\n");
    sequenceWindowServer(&client_hello2, &client_key_exchange, &client_change_cipher);
  }
  else if (strcmp(rule, "rseqwindowing-client") == 0)
  {
    experimenttype = CLIENT;
    approach = CONFORMANCE;
    printf("Record Sequence Number Rule 3 for the Client Side:\n\n");
    sequenceWindowClient(&hello_verify_request, &server_hello, &server_hello_done, &server_change_cipher);
  }
  else if (strcmp(rule, "rlenvalidity-server") == 0)
  {
    experimenttype = SERVER;
    approach = CONFORMANCE;
    printf("Record Length Validity Server Rule:\n\n");
    recordLengthValidityServer(&client_hello1, &client_hello2, &client_key_exchange, &client_change_cipher,
                               &shadow_client_hello1, &shadow_client_hello2, &shadow_client_key_exchange, &shadow_client_change_cipher);
  }
  else if (strcmp(rule, "rlenvalidity-client") == 0)
  {
    experimenttype = CLIENT;
    approach = CONFORMANCE;
    printf("Record Length Validity Client Rule:\n\n");
    recordLengthValidityClient(&hello_verify_request, &server_hello, &server_hello_done, &server_change_cipher,
                               &shadow_hello_verify_request, &shadow_server_hello, &shadow_server_hello_done, &shadow_server_change_cipher);
  }
  ///////////////////////// Fragment-Level Constraints

  else if (strcmp(rule, "htypevalidity-server") == 0)
  {
    experimenttype = SERVER;
    approach = CONFORMANCE;
    printf("Handshake Type Server Rule:\n\n");
    handshakeTypeValidityServer(&client_hello1, &client_hello2, &client_key_exchange,
                                &shadow_client_hello1, &shadow_client_hello2, &shadow_client_key_exchange);
  }

  else if (strcmp(rule, "htypevalidity-client") == 0)
  {
    experimenttype = CLIENT;
    approach = CONFORMANCE;
    printf("Handshake Type Client Rule:\n\n");
    handshakeTypeValidityClient(&hello_verify_request, &server_hello, &server_hello_done,
                                &shadow_hello_verify_request, &shadow_server_hello, &shadow_server_hello_done);
  }

  else if (strcmp(rule, "mlenvalidity-server") == 0)
  {
    experimenttype = SERVER;
    approach = CONFORMANCE;
    printf("Message Length Validity Server Rule:\n\n");
    messageLengthValidityServer(&client_hello1, &client_hello2, &client_key_exchange,
                                &shadow_client_hello1, &shadow_client_hello2, &shadow_client_key_exchange);
  }
  else if (strcmp(rule, "mlenvalidity-client") == 0)
  {
    experimenttype = CLIENT;
    approach = CONFORMANCE;
    printf("Message Length Validity Client Rule:\n\n");
    messageLengthValidityClient(&hello_verify_request, &server_hello, &server_hello_done,
                                &shadow_hello_verify_request, &shadow_server_hello, &shadow_server_hello_done);
  }
  else if (strcmp(rule, "mseqvalidity-server") == 0)
  {
    experimenttype = SERVER;
    approach = CONFORMANCE;
    printf("Message Sequence Number Rule for the Server Side:\n\n");
    messageSequenceServer(&client_hello1, &client_hello2, &client_key_exchange);
  }
  else if (strcmp(rule, "mseqvalidity-client") == 0)
  {
    experimenttype = CLIENT;
    approach = CONFORMANCE;
    printf("Message Sequence Number Rule for the Client Side:\n\n");
    messageSequenceClient(&hello_verify_request, &server_hello, &server_hello_done);
  }
  else if (strcmp(rule, "flenvalidity-server") == 0)
  {
    experimenttype = SERVER;
    approach = CONFORMANCE;
    printf("Fragment Length Validity Server Rule:\n\n");
    fragmentLengthValidityServer(&client_hello1, &client_hello2, &client_key_exchange,
                                 &shadow_client_hello1, &shadow_client_hello2, &shadow_client_key_exchange);
  }
  else if (strcmp(rule, "flenvalidity-client") == 0)
  {
    experimenttype = CLIENT;
    approach = CONFORMANCE;
    printf("Fragment Length Validity Client Rule:\n\n");
    fragmentLengthValidityClient(&hello_verify_request, &server_hello, &server_hello_done,
                                 &shadow_hello_verify_request, &shadow_server_hello, &shadow_server_hello_done);
  }
  else if (strcmp(rule, "fragoffset-server") == 0)
  {
    experimenttype = SERVER;
    approach = CONFORMANCE;
    puts("Fragment Offset Validity Server Rule:\n");
    offsetValidityNoFragServer(&client_hello1, &client_hello2, &client_key_exchange);
  }
  else if (strcmp(rule, "fragoffset-client") == 0)
  {
    experimenttype = CLIENT;
    approach = CONFORMANCE;
    puts("Fragment Offset Validity Client Rule:\n");
    offsetValidityNoFragClient(&hello_verify_request, &server_hello, &server_hello_done);
  }
  else if (strcmp(rule, "flenmleneq-server") == 0)
  {
    experimenttype = SERVER;
    approach = CONFORMANCE;
    puts("Fragment Length Message Length Equality Server Rule:\n");
    fragLenMessageLenEqualityServer(&client_hello1, &client_hello2, &client_key_exchange);
  }
  else if (strcmp(rule, "flenmleneq-client") == 0)
  {
    experimenttype = CLIENT;
    approach = CONFORMANCE;
    puts("Fragment Length Message Length Equality Client Rule:\n");
    fragLenMessageLenEqualityClient(&hello_verify_request, &server_hello, &server_hello_done);
  }
  /////////////////////// Message-Level Constraints
  else if (strcmp(rule, "handversion-server") == 0)
  {
    experimenttype = SERVER;
    approach = CONFORMANCE;
    printf("Handshake Version Server Rule:\n\n");
    handshakeVersionServer(&client_hello1, &client_hello2);
  }
  else if (strcmp(rule, "handversion-client") == 0)
  {
    experimenttype = CLIENT;
    approach = CONFORMANCE;
    printf("Handshake Version Client Rule:\n\n");
    handshakeVersionClient(&hello_verify_request, &server_hello);
  }

  else if (strcmp(rule, "cookielenvalidity-server") == 0)
  {
    experimenttype = SERVER;
    approach = CONFORMANCE;
    printf("Cookie Length Rule Server:\n\n");
    cookieLengthServer(&client_hello1, &client_hello2, &shadow_client_hello1,
                       &shadow_client_hello2);
  }
  else if (strcmp(rule, "cookielenvalidity-client") == 0)
  {
    experimenttype = CLIENT;
    approach = CONFORMANCE;
    printf("Cookie Length Rule Client:\n\n");
    cookieLengthClient(&hello_verify_request, &shadow_hello_verify_request);
  }
  else if (strcmp(rule, "sessionidlenvalidity-server") == 0)
  {
    experimenttype = SERVER;
    approach = CONFORMANCE;
    printf("SessionID Rule Server:\n\n");
    sessionIDLengthServer(&client_hello1, &client_hello2, &shadow_client_hello1,
                          &shadow_client_hello2);
  }
  else if (strcmp(rule, "sessionidlenvalidity-client") == 0)
  {
    experimenttype = CLIENT;
    approach = CONFORMANCE;
    printf("SessionID Rule Client:\n\n");
    sessionIDLengthClient(&server_hello, &shadow_server_hello);
  }
  else if (strcmp(rule, "hvrmseqoutput-server") == 0)
  {
    experimenttype = SERVER;
    approach = CONFORMANCE;
    ruletype = OUTPUT;
    printf("HelloVerifyRequest Message Sequence Output Rule Server:\n\n");
    hvrMessageSequenceOutputServer(&client_hello1, &output_req);
  }
  else if (strcmp(rule, "shmseqoutput-server") == 0)
  {
    experimenttype = SERVER;
    approach = CONFORMANCE;
    ruletype = OUTPUT;
    printf("ServerHello Message Sequence Output Rule Server:\n\n");
    shMessageSequenceOutputServer(&client_hello2, &output_req);
  }
  else if (strcmp(rule, "CH0") == 0)
  {
    approach = SYMBEX;
    printf("Symbolically Executing ClientHello:\n\n");
    symbolicClientHello1(&client_hello1, &shadow_client_hello1);
  }
  else if (strcmp(rule, "CH2") == 0)
  {
    approach = SYMBEX;
    printf("Symbolically Executing ClientHello2:\n\n");
    symbolicClientHello2(&client_hello2, &shadow_client_hello2);
  }
  else if (strcmp(rule, "CKE") == 0)
  {
    approach = SYMBEX;
    printf("Symbolically Executing ClientKeyExchange:\n\n");
    symbolicClientKeyExchange(&client_key_exchange, &shadow_client_key_exchange);
  }
  else if (strcmp(rule, "CCC") == 0)
  {
    approach = SYMBEX;
    printf("Symbolically Executing ChangeCipherSuite(S):\n\n");
    symbolicClientChangeCipher(&client_change_cipher, &shadow_client_change_cipher);
  }
  else if (strcmp(rule, "CFI") == 0)
  {
    approach = SYMBEX;
    printf("Symbolically Executing Finished(S):\n\n");
    symbolicClientFinished(&client_finished, &shadow_client_finished);
  }
  else if (strcmp(rule, "HVR") == 0)
  {
    approach = SYMBEX;
    printf("Symbolically Executing HelloVerifyRequest:\n\n");
    symbolicHelloVerifyRequest(&hello_verify_request, &shadow_hello_verify_request);
  }
  else if (strcmp(rule, "SH") == 0)
  {
    approach = SYMBEX;
    printf("Symbolically Executing ServerHello:\n\n");
    symbolicServerHello(&server_hello, &shadow_server_hello);
  }
  else if (strcmp(rule, "SHD") == 0)
  {
    approach = SYMBEX;
    printf("Symbolically Executing ServerHelloDone:\n\n");
    symbolicServerHelloDone(&server_hello_done, &shadow_server_hello_done);
  }
  else if (strcmp(rule, "SCC") == 0)
  {
    approach = SYMBEX;
    printf("Symbolically Executing ChangeCipherSuite(C):\n\n");
    symbolicServerChangeCipher(&server_change_cipher, &shadow_server_change_cipher);
  }
  else if (strcmp(rule, "SFI") == 0)
  {
    approach = SYMBEX;
    printf("Symbolically Executing Finished(C):\n\n");
    symbolicServerFinished(&server_finished, &shadow_server_finished);
  }
  else if (strcmp(rule, "None") == 0)
  {
    approach = None;
    printf("Normal Execution!\n\n");
  }
  else
  {
    printf("Rule is not defined!\n\n");
    printusage("./dtls-fuzz");
    return 0;
  }

#endif

  return 0;
}

int main(int argc, char **argv)
{
  FILE *f = NULL;
  static uint8_t buf[MAX_READ_BUFF];
  char rule[100];
  int packet_order = -1, len = 0;

  int opt;      // Parsing program arguments
  int iarg = 0; //input argument
  int rarg = 0; //Rule argument
  int narg = 0; //Normal execution Argument + log records

  while ((opt = getopt(argc, argv, "i:r:n")) != -1)
  {
    switch (opt)
    {
    case 'i':
      iarg = 1;
      strcpy(PSK_HANDSHAKE_FOLDER, optarg);
      // strcpy(PSK_HANDSHAKE_FOLDER, "handshakes/nofrag");
      break;
    case 'r':
      rarg = 1;
      strcpy(rule, optarg);
      // strcpy(rule, "none");
      break;
    case 'n':
      narg = 1;
      dump_output_mode = 1;
      break;
    default:
      printusage(argv[0]);
      break;
    }
  }
  dtls_set_log_level(6);
  if (iarg == 0 && rarg == 0 && isFragmented == 0 && narg == 1)
  {
    perform_sequential_handshake(buf, len, "psk", packet_order);
  }
  else if (iarg == 1 && rarg == 1 && narg == 0)
  {
    int result = 0;
    result = load_psk_packets("psk", rule);
    if (result != 0)
    {
      printf("Error Loading records!\n");
      goto error;
    }
    if (isFragmented)
    {
      perform_unified_handshake_frag();
    }
    else
    {
      result = perform_unified_handshake_nofrag();
      if (result != 0)
      {
        printf("Handshake Failure!: return: %d\n", result);
        goto error;
      }
    }
  }
  else
  {
    printusage(argv[0]);
  }

error:
  if (f != NULL)
  {
    fclose(f);
  }

  return 0;
}
