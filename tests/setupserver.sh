readonly server="./dtls-server"

function compile() {
    if [[ ! -e $server ]]
    then
        cd ../
        make
        cd tests
        echo -e "\n\n"
    else
        echo "It is already built!"
    fi
}
function setup_server() {
    if [[ -e $server ]]
    then 
        echo "Running dtls-server on port 20220"
        ./dtls-server -p 20220 -v 6
    else
        echo "./dtls-server does not exist!"
    fi
}

compile
setup_server