
#ifndef Symbolic_DTLS
#define Symbolic_DTLS

#include <klee/klee.h>
#include "records.h"

typedef void (*OutputAssertionServer)(CH0 *client_hello1 , CH2 *client_hello2, CKE *client_key_exchange, CCS *change_cipher_spec, int rec_index, char *response);

typedef  struct OutputRequirement {
    OutputAssertionServer serverAssertion;
    int step; 
} OutputRequirement;

void hvrMessageSequenceOutputServer(CH0 *client_hello1, OutputRequirement *output_requirement);
void shMessageSequenceOutputServer(CH2 *client_hello2, OutputRequirement *output_requirement);
void checkOutputRequirement(CH0 *client_hello1 , CH2 *client_hello2, CKE *client_key_exchange, CCS *change_cipher_spec, char *response, OutputRequirement *requirement);

////////////////
void contentTypeServer(CH0 *client_hello1, CH2 *client_hello2, CKE *client_key_exchange, CCS *client_change_cipher);

void contentTypeClient(HVR *hello_verify_request, SH *server_hello, SHD *server_hello_done, CCS *server_change_cipher);

void recordVersionServer(CH2 *client_hello2, CKE *client_key_exchange, CCS *client_change_cipher);

void recordVersionClient(HVR *hello_verify_request, SH *server_hello, SHD *server_hello_done, CCS *server_change_cipher);

void epochServer(CH0 *client_hello1, CH2 *client_hello2, CKE *client_key_exchange, CCS *client_change_cipher);

void epochClient(HVR *hello_verify_request, SH *server_hello, SHD *server_hello_done, CCS *server_change_cipher);

void sequenceUniquenessServer(CH2 *client_hello2, CKE *client_key_exchange, CCS *client_change_cipher);

void sequenceUniquenessClient(HVR *hello_verify_request, SH *server_hello, SHD *server_hello_done, CCS *server_change_cipher);

void sequenceWindowServer(CH2 *client_hello2, CKE *client_key_exchange, CCS *client_change_cipher);

void sequenceWindowClient(HVR *hello_verify_request, SH *server_hello, SHD *server_hello_done, CCS *server_change_cipher);

void recordLengthValidityServer(CH0 *client_hello1, CH2 *client_hello2, CKE *client_key_exchange,
                                CCS *client_change_cipher, CH0 *shadow_client_hello1, CH2 *shadow_client_hello2, CKE *shadow_client_key_exchange,
                                CCS *shadow_client_change_cipher);

void recordLengthValidityClient(HVR *hello_verify_request, SH *server_hello, SHD *server_hello_done,
                                CCS *server_change_cipher, HVR *shadow_hello_verify_request, SH *shadow_server_hello, SHD *shadow_server_hello_done,
                                CCS *shadow_server_change_cipher);

void handshakeTypeValidityServer(CH0 *client_hello1, CH2 *client_hello2, CKE *client_key_exchange,
                                 CH0 *shadow_client_hello1, CH2 *shadow_client_hello2, CKE *shadow_client_key_exchange);

void handshakeTypeValidityClient(HVR *hello_verify_request, SH *server_hello, SHD *server_hello_done,
                                 HVR *shadow_hello_verify_request, SH *shadow_server_hello, SHD *shadow_server_hello_done);

void messageLengthValidityServer(CH0 *client_hello1, CH2 *client_hello2, CKE *client_key_exchange,
                                 CH0 *shadow_client_hello1, CH2 *shadow_client_hello2, CKE *shadow_client_key_exchange);

void messageLengthValidityClient(HVR *hello_verify_request, SH *server_hello, SHD *server_hello_done,
                                 HVR *shadow_hello_verify_request, SH *shadow_server_hello, SHD *shadow_server_hello_done);

void messageSequenceServer(CH0 *client_hello1, CH2 *client_hello2, CKE *client_key_exchange);

void messageSequenceClient(HVR *hello_verify_request, SH *server_hello, SHD *server_hello_done);

void fragmentLengthValidityServer(CH0 *client_hello1, CH2 *client_hello2, CKE *client_key_exchange,
                                  CH0 *shadow_client_hello1, CH2 *shadow_client_hello2, CKE *shadow_client_key_exchange);

void fragmentLengthValidityClient(HVR *hello_verify_request, SH *server_hello, SHD *server_hello_done,
                                  HVR *shadow_hello_verify_request, SH *shadow_server_hello, SHD *shadow_server_hello_done);

void offsetValidityNoFragServer(CH0 *client_hello1, CH2 *client_hello2, CKE *client_key_exchange);

void offsetValidityNoFragClient(HVR *hello_verify_request, SH *server_hello, SHD *server_hello_done);

void fragLenMessageLenEqualityServer(CH0 *client_hello1, CH2 *client_hello2, CKE *client_key_exchange);

void fragLenMessageLenEqualityClient(HVR *hello_verify_request, SH *server_hello, SHD *server_hello_done);

void byteContainedFragServer(CKEFRAG *client_key_exchange);

void byteContainedFragClient(SHFRAG *server_hello);

void byteContainedFragServerTiny(CKEf1 *client_key_exchangef1, CKEf2 *client_key_exchangef2);

void byteContainedFragClientTiny(SHf1 *server_hellof1, SHf2 *server_hellof2);

void messageSequenceEqualityFragServer(CKEFRAG *client_key_exchange);

void messageSequenceEqualityFragClient(SHFRAG *server_hello);

void messageSequenceEqualityFragServerTiny(CKEf1 *client_key_exchangef1, CKEf2 *client_key_exchangef2);

void messageSequenceEqualityFragClientTiny(SHf1 *server_hellof1, SHf2 *server_hellof2);

void MessageLengthFragEqualityServer(CKEFRAG *client_key_exchange);

void MessageLengthFragEqualityClient(SHFRAG *server_hello);

void MessageLengthFragEqualityServerTiny(CKEf1 *client_key_exchangef1, CKEf2 *client_key_exchangef2);

void MessageLengthFragEqualityClientTiny(SHf1 *server_hellof1, SHf2 *server_hellof2);

void handshakeVersionServer(CH0 *client_hello1, CH2 *client_hello2);

void handshakeVersionClient(HVR *hello_verify_request, SH *server_hello);

void cookieLengthServer(CH0 *client_hello1, CH2 *client_hello2, CH0 *shadow_client_hello1,
                        CH2 *shadow_client_hello2);

void cookieLengthClient(HVR *hello_verify_request, HVR *shadow_hello_verify_request);

void sessionIDLengthServer(CH0 *client_hello1, CH2 *client_hello2, CH0 *shadow_client_hello1,
                           CH2 *shadow_client_hello2);

void sessionIDLengthClient(SH *server_hello, SH *shadow_server_hello);

void extensionLengthValidityServer(CH0 *client_hello1, CH2 *client_hello2, CH0 *shadow_client_hello1,
                           CH2 *shadow_client_hello2);

void extensionLengthValidityClient(SH *server_hello, SH *shadow_server_hello);                           

///////////

void symbolicClientHello1(CH0 *client_hello1, CH0 *shadow_client_hello1);

void symbolicClientHello2(CH2 *client_hello2, CH2 *shadow_client_hello2);

void symbolicClientKeyExchange(CKE *client_key_exchange, CKE *shadow_client_key_exchange);

void symbolicClientChangeCipher(CCS *client_change_cipher, CCS *shadow_client_change_cipher);

void symbolicClientFinished(FIN *client_finished, FIN *shadow_client_finished);

void symbolicHelloVerifyRequest(HVR *hello_verify_request, HVR *shadow_hello_verify_request);

void symbolicServerHello(SH *server_hello, SH *shadow_server_hello);

void symbolicServerHelloDone(SHD *server_hello_done, SHD *shadow_server_hello_done);

void symbolicServerChangeCipher(CCS *server_change_cipher, CCS *shadow_server_change_cipher);

void symbolicServerFinished(FIN *server_finished, FIN *shadow_server_finished);

/////////////////////////////////////////////////////////

#endif /* Symbolic */


